int get_min(int* arr, int size_of_array){
  if(size_of_array == 1){
    return arr[0];
  }
  int min_element;
  for(int i = 0; i < size_of_array-1; i++){
    min_element = min(arr[i], arr[i+1]);
  }
  return(min_element);
}

int mm_to_sixteenths(float mm){
  return (int)((mm/SCREW_PITCH_MM)*(FULL_REVOLUTION_DEGREES/ONE_SIXTEENTH_STEP_DEGREES));
}

float sixteenths_to_mm(int sixteenths){
  return (sixteenths*(ONE_SIXTEENTH_STEP_DEGREES/FULL_REVOLUTION_DEGREES)*SCREW_PITCH_MM);
}

long long int mm_s_to_us_step(float mm_s)
{
  float rev_s = mm_s/3;
  float steps_s = rev_s*20.0*16.0;
  long long int us_step = 1000000.0/steps_s;
  return us_step;
}

float us_step_to_mm_s(long long int us_step)
{
  float steps_s = 1000000.0/us_step;
  float rev_s = steps_s/(20.0*16.0);
  float mm_s = 3*rev_s;
  return mm_s;
}

void move_ramping_manual(float mm, float spd_mm_s){

  int num_intervals = spd_mm_s/acceleration_step_mm_s;
  int interval_time_us = (acceleration_total_time_ms*1000.0)/num_intervals;
  int cur_interval = 0;
  
  long int total_steps = mm_to_sixteenths(mm);
  long int steps_taken = 0;
  
  long long int tim_us_1 = micros();
  long long int tim_us_2 = tim_us_1;
  long int tim_ms = millis();
  long int start_time = tim_ms;

  long long int us_delay = mm_s_to_us_step(0.00000001);
  long long int top_us_delay = mm_s_to_us_step(spd_mm_s);
  
  while(us_delay > top_us_delay && tim_ms-start_time < acceleration_total_time_ms)
  {
    tim_ms = millis();
    if(micros() - tim_us_1 >= (us_delay/2))
    {
      digitalWrite(stp, !digitalRead(stp));
      if(digitalRead(stp) == HIGH){
        ++steps_taken;
      }
      tim_us_1 = micros();
    }
    if(micros() - tim_us_2 >= interval_time_us)
    {
      ++cur_interval;
      us_delay = mm_s_to_us_step(cur_interval*acceleration_step_mm_s);
      tim_us_2 = micros();
    }
  }
  
  long int stage_1_steps = steps_taken;
  
  while(steps_taken < total_steps - stage_1_steps)
  {
    if(micros() - tim_us_1 >= (top_us_delay/2))
    {
      digitalWrite(stp, !digitalRead(stp));
      if(digitalRead(stp) == HIGH){
        ++steps_taken;
      }
      tim_us_1 = micros();
    }
  }
  
  start_time = millis();
  tim_us_2 = micros();
  tim_ms = start_time;
  
  while(steps_taken < total_steps && tim_ms - start_time < acceleration_total_time_ms)
  {
    tim_ms = millis();
    if(micros() - tim_us_1 >= (us_delay/2))
    {
      digitalWrite(stp, !digitalRead(stp));
      if(digitalRead(stp) == HIGH){
        ++steps_taken;
      }
      tim_us_1 = micros();
    }
    if(micros() - tim_us_2 >= interval_time_us)
    {
      --cur_interval;
      if(cur_interval < 1)
      {
        cur_interval = 1;
      }
      us_delay = mm_s_to_us_step(cur_interval*acceleration_step_mm_s);
      tim_us_2 = micros();
    }
  }
}

void change_dir(){
  digitalWrite(dir, !digitalRead(dir));
}

void split_string(String s, char delimiter, String* ss){
  int split_index = 0;
  int count = 0;
  for(int i = 0; i < s.length(); i++){
    if(s[i] == delimiter){
      ss[count] = s.substring(split_index, i);
      ++count;
      split_index = i+1;
    }
  }
}

void execute(String cmd, String* ss){
  split_string(cmd, ' ', ss);
  char command_character = ss[0].charAt(0);
  if(command_character == 'm')
  {
    float dist_mm = ss[1].toFloat();
    float spd_pct = ss[2].toFloat();
    move_ramping_manual(dist_mm, spd_pct);
  }
  else if(command_character == 'd')
  {
    change_dir();
  }
  else if(command_character == 'p')
  {
    acceleration_total_time_ms = ss[1].toInt();
  }
  else if(command_character == 'a')
  {
    acceleration_step_mm_s = ss[1].toFloat();
  }
  else if(command_character == 'g')
  {
    for(int i = 0; i < n; i++)
    {
      move_ramping_manual(travel_dist, top_spd);
      change_dir();
    }
  }
  else if(command_character == 'n')
  {
    n = ss[1].toInt();
  }
  else if(command_character == 's')
  {
    top_spd = ss[1].toFloat();
  }
  else if(command_character == 'x')
  {
    travel_dist = ss[1].toFloat();
  }
}
