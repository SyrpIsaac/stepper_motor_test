/* Stepper Motor test sketch by Isaac Middlemiss
 *  
 *  d - change direction of movement
 *  
 *  a [num1]- set acceleration resolution in mm/s
 *  e.g. 'a 4'
 *  
 *  p - sets acceleration period in ms, ie time taken to accelerate to desired speed
 *  
 *  m [num1] [num2] - move num1 mm at top speed of num2 mm/s
 *  e.g. 'm 50 10'
 *  
 *  n [num1] - sets number of times to repeat set motion
 *  e.g. 'n 8'
 *  
 *  s [num1] - set speed in mm/s for set motion
 *  e.g. 's 50'
 *  
 *  x [num1] - set travel distance for set motion
 *  e.g. 'x 60'
 *  
 *  g - execute set motion according to set parameters
 */
#define SCREW_PITCH_MM 3.0
#define SCREW_LENGTH_MM 74.0
#define STROKE_MM 64.0
#define FULL_STEP_DEGREES 18.0
#define FULL_REVOLUTION_DEGREES 360.0
#define ONE_SIXTEENTH_STEP_DEGREES 1.125

int dir = 32; 
int stp = 33;

int m1 = 25;
int m2 = 26;
int m3 = 27;

float acceleration_step_mm_s = 0.01;
float travel_dist = 0;
float top_spd = 0;
int acceleration_total_time_ms = 100;
int n = 1;

String command = "";
String split_command[4];

char incoming;
String serial_out = "";

void setup() {
  pinMode(dir, OUTPUT);
  pinMode(stp, OUTPUT);
  pinMode(m1, OUTPUT);
  pinMode(m2, OUTPUT);
  pinMode(m3, OUTPUT);
  digitalWrite(m1, HIGH);
  digitalWrite(m2, HIGH);
  digitalWrite(m3, HIGH);
  digitalWrite(dir, LOW);
  Serial.begin(115200);
  Serial.println("Program started. Current direction: towards motor");
  
}

void loop() {
  while(Serial.available()){
    incoming = Serial.read();
    if(incoming != '\r' && incoming != '\n'){
      command += incoming;
    }else{
      Serial.println(String("->") + command);
      if(command.length() > 0){
        command.trim();
        execute(command+' ', split_command);
        Serial.println(serial_out+"Number of motions('n'): " + n + "\t Acceleration/deceleration period in ms('p'): " + acceleration_total_time_ms + "\tDistance in mm('x'): " + travel_dist + "\tTop speed in mm/s('s'): " + top_spd + "\tAcceleration resolution in mm/s('a'): " + acceleration_step_mm_s);
        command = "";
      }
    }
  }
}
